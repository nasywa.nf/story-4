from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.portofolio, name='portofolio'),
    path('profil/', views.profile, name='profil'),
    
]